---
marp: false
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

## Restitution des travaux de la Séance 1 du Conseil Scientifique de la DAPI
### Ph. Dessus & F. Ménard
### 13/11/2023

---
<!-- paginate: true -->
<!-- footer: Dessus & Ménard - Restitution Travaux S1 CS DAPI UGA - 13/11/23 CC:BY-NC-SA -->


# Modalités de travail : Questions de réflexion

- Dans les 4 prochaines années, qu'est-ce qu'on voudrait voir avancer à l'UGA   
  1/ en pédagogie universitaire ? 2/ en usages du numérique  ? et comment s'y prendre ? 
- **(15 min) Individuellement**, mentionner sur des papiers-notes repositionnables des idées sur ces 2 questions (1 idée par question) 
- **(20 min)** **Par petits groupes de 2-3** (si possible pluri-disciplinaires), tri des idées.
- **(15 min)** Mise en collectif et discussion

---
#  Plan

1. Technique et admin
2. Formation
3. Pédagogie
4. Politique
5. Synthèse

---
# :one: Technique et admin

---
# 1. Salles de cours

- Pouvoir disposer de salles de cours modulables ou plus de mutualisation de celles qui existent. Possiblement utilisables en bi-modalité, adaptées à la pédagogie active
- Installer des salles équipées pour la télécollaboration (projets entre étudiants à l’international). Bonne connexion, bons casques isolant du bruit ambiant + micros. Si possible ordinateurs avec webcams disposés en U, murs et panneaux inscriptibles. (Postes individuels pour 20 à 30 étudiants) :arrow_right: Comité d'usagers

---
# 1. Plates-formes

- Comment rendre plus souple et plus fluide l’usage des plates-formes numériques (Moodle, OnlyOffice, Cloud UGA) pour une meilleure appropriation par les enseignants ? 
  - Dépôt d’une vidéo sur Moodle oblige à passer par une plate-forme tierce (pod)
  - Mise en forme d’un cours ou d’un test sur Moodle
- Ergonomie “repoussante” pour certains collègues de certaines applis Moodle (perte de travail si absence de validation à chaque étape dans module test)
- :arrow_right: Création d'un guichet unique pour les plates-formes péda ?

---
# :two: Volet formation

---
# 2. Méthodes de travail à l'entrée dans l'université

- Modules d'aide à la prise de notes L1
- Méthodes de travail universitaire à acquérir plus efficacement (les guider, pas seulement leur enseigner). Ex :
  - Reposer sur semaines d’accueil + tutorat 
  - Mettre à disposition des ressources mutualisées pour éviter infos dépassées et approximations

:arrow_right: Actuellement offre pas très harmonisée : parfois faite par la BU, les ETC..., parfois dans/hors maquette, sans ECTS correspondants. Le frein principal : temps étudiant non extensible à l'infini. Devrait être fait systématiquement dans toutes les composantes en L1

---
# 2. Formation au numérique

- Réduire la fracture numérique chez les étudiants (et enseignants) :arrow_right: Systématiser les prêts de matériel dans les composantes ; développer la plate-forme *ebureau* (machine virtuelle via navigateur)

- Mieux apprendre à exploiter les ressources numériques en cours par les étudiants. :arrow_right: - Archivage, tri, marquage des éléments traités - Cartographie “centralisée” des ressources et documents de référence - Quels outils pour prise de notes ? Présentation de *LabNBook*

- Multiplier les techniques dans chaque enseignement diapo/capsules vidéo

---
# 2. Mutualisation des cours transversaux 

- Par exemple : cours d'informatique, de langues, de statistiques, de simulation de situations professionnelles, quand bien sûr le domaine s'y prête :arrow_right: Mise en place d'une Communauté de pratique ?

---
# 2. Évaluations

- Comment rendre utiles et utilisables les évaluations (et tout ce qu’il y a autour) pour les étudiant·es dans leur formation ? :arrow_right: Les notes ne devraient pas être le seul feedback.  *LabNbook* a un nouvel outil de grilles critériées plus évolué que celui de Moodle
- Avoir un exerciseur global pour l'ensemble des cours favorisant des contrôles continus corrigés automatiquement :arrow_right: Travailler à un cahier des charges pour des banques de questions mutualisées (Moodle/H5P) 
- Repenser les productions “à la maison“ à la lumière de l'IA générative

---
# 2. Formations internationales à distance

- Faire des **formations entièrement à distance** avec des experts internationaux

---
# :three: Pédagogie

---
# 3. Interactions entre étudiants

- Favoriser les pratiques basées sur les interactions entre étudiants (à distance ou en présence) et les aider à mettre en place les stratégies collaboratives nécessaires (selon contexte, CM, TD, Distance), selon taille de promos, avantages/inconvénients :arrow_right: Voir formation DAPI “**Accompagnement méthodo des étudiants pour soutenir leur réussite**“. Voir Élodie pour creuser ce contenu dans les formations DAPI

---
# 3. Communautés de pratiques

- Développer de nouvelles CdP (utilisation de l'IA générative) ou relancer les CdP passées (hybridation, APP, classe inversée) ?

---
# 3. Formation par la simulation

- Lorsque pertinent, réfléchir à mettre en place des formations par la simulation (jeux de rôles avec étudiant·es entraîné·es), reprenant la formation “clinique“ faite en médecine

---
# 3. Changer l'état d'esprit des EC sur la pédagogie

Les EC s'investissant dans la pédagogie sont parfois mal vus, puisque ne contribuant plus directement à l'effort de recherche dans leur discipline :arrow_right: Lien avec la reconnaissance du travail pédagogique ? *Scholarship of Teaching and Learning*. Depuis quelques années, un peu plus facile de publier dans des revues “standard” des travaux en lien avec la pédagogie. En interne Idex Formation ; congé projet pédagogique

---
# 3. Accompagnement pédagogique

- Plus de proximité dans l'accompagnement pédagogique :arrow_right: Complexe à mettre en place : gestion du temps et de la charge de travail des ingé péda. dans les composantes (plutôt y mettre des techniciens-ASI formés que des ingénieur·es)

---
# :four: Politique

--- 
# 4. Financement et appui des plates-formes locales

- Faire entrer dans le périmètre DSIM les outils développés localement (*Caséine*, *LabNBook*), pour signifier un appui institutionnel et consolider leur statut

---
# 4. Outils libres et gratuits vs. propriétaires

- Difficile d'en faire un choix politique universel, car cela dépend des pratiques de formation et des usages de chacun, mais, dans la mesure du possible, privilégier le choix des logiciels libres. :arrow_right: Voir comment la DAPI pourrait assister ce choix ?

---
# 4. Réduire le nombre de LMS différents à l'UGA

- Réflexion en cours

---
# :five: Synthèse

---
# 5. Synthèse rapide

1. Mutualisation
   - des salles, notamment celles utilisables en hybride
   - de l'aide sur les plates-formes
   - de certaines offres de formation (info, MTU : méthode de travail univ., etc.)
   - de techniciens ASI pour l'accompagnement péda. de proximité
2. Formation
   - repenser l'évaluation avec l'arrivée de l'IA générative
   - renforcer les CdP
   - diffuser la formation par simulation

---
# :six: Et après ?

---
# Et après ?

- Difficile de projeter l'action du CS, puisque des élections à la présidence peuvent changer les orientations politiques
- Si le CS est maintenu, actions dans ces directions :
  - produire **un livre blanc** pour un plan stratégique 
  - donner, en cours de mandat et à un rythme annuel, des **pistes de travail** prospectives et des orientations dans le domaine de la pédagogie universitaire et de l'intégration du numérique ;
  -  donner ponctuellement des **avis évaluatifs** sur des projets pédagogiques
  -  participer annuellement à un **atelier scientifique** thématique

---
# Remerciements

- Merci aux membres du Conseil scientifique de la DAPI
- Merci à David Déchenaud pour son aide dans la mise en place du Conseil