---
marp: true
paginate: true
autoscale: true
theme: uga
---
<!-- _paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# Conseil scientifique de la DAPI : 2e réunion
## DAPI, UGA

6 décembre 2024

---
<!-- footer: Conseil scientifique DAPI 2e réunion | 6/12/24 | CC:BY-NC-SA -->
<!-- paginate: true -->

- URL de cette présentation : https://link.infini.fr/dapi-cs2

---

# :two: Ordre du jour de cette réunion (phd) 

1. Mot de David Déchenaud
2. But de la réunion
3. Brainstorming et échanges pour le travail thématique à venir (Livre blanc)
4. Les effets du travail du CS (FM)
5. Brainstorming (suite et fin)
6. Point sur le fonctionnement du CS
7. Présentation du projet d'US CNP (FM)

---
# :one: Mot de bienvenue de David Déchenaud, VP formation UGA


---
# Personnes excusées

- Philippe Chaffanjon
- Mathilde Loretz
- Virginie Zampa

---
# :two: But : Détermination de thèmes de travail pour le livre blanc (phd)

- Choix de thèmes de travail pour l'année, circonscrits et applicables à l'UGA…
- … pour l'amélioration de la **qualité des formations de l'UGA**
- Servira à l'équipe du VP formation pour la prochaine **accréditation des maquettes**
- Ne pas viser l'exhaustivité, mais **se servir des compétences** des membres du CS
- Possible de s'adjoindre des collègues hors-CS
- But final : Alimenter un “**livre blanc de la pédagogie universitaire (dans le contexte UGA)**”, qui serait publié en accès ouvert
  
---
# :three: Organisation du Brainstorming (phd)
<!-- _class: t-60 --> 

1. **15 min** : Chacun.e écrit une liste de 5 sujets **précis** sur *post-it* (avec nom/prénom) sur lesquelle il ou elle est expert.e (ou motivé.e) et pourrait contribuer à écrire un chapitre 
2. Quelques rappels et infos pour stimuler les idées
  - **10 min** Présentation de la Réunion 1 du CS-DAPI et effets  sur le travail de la DAPI
  - **10 min** Thématiques de travail en cours de la DAPI
  - **10 min** Un exemple : les rapports “*Innovating pedagogy*”
3. **45 min** En groupes aléatoires et interdisciplinaires. Examen de tous les sujets (du groupe, écrits en 1.) pour arriver à une liste d'une quinzaine de sujets max. :arrow_right: priorisation collective
4. **30 min** Répartition d'un sujet par binôme (à la fois) et réflexion sur le plan et avancement du projet, possibles collaborateurs et échéancier

---
# :four: Les effets du CS (1re réunion) (1/2) (fm)
- La démarche a été “*bottom-up*”, où le CS a fait un état des lieux des forces et faiblesses de la pédagogie universitaire au niveau UGA, et a suggéré des pistes d'amélioration et de travail
- Ces pistes ont été utiles pour l'action de la DAPI, présente et à venir
  
---
# :four: Les effets du CS (1re réunion) (2/2) (fm)

- Cf. [présentation](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-dapi/Analyse-S1-CS-DAPI.pdf) du bilan (déjà adressée)

---
# :four: Effets du CS (fm)

## 1. Technique et admin
1. Salles de cours
  :arrow_right: Comité d'usagers, Salle CLV, Cartographie, Actions H3S:(ammphi G), projets en cours)
1. Plates-formes
  :arrow_right:  Moodle 4, Création d'un guichet unique pour les plates-formes péda ?

---

## 2. Volet formation (fm)
1. Méthodes de travail à l'entrée dans l'université
:arrow_right: Travail en projet avec soutien de FlexiTLV

## 3. Politique

1. Financement et appui des plates-formes locales (*Caséine*, *LabNBook*)
:arrow_right: Réflexion en cours avec les VP et l'IDEX
1. Réduire le nombre de LMS différents à l'UGA
:arrow_right: Réflexion en cours avec les VP

---
<!-- _class: t-60 --> 

## 1. Pédagogie
1. Interactions entre étudiants
:arrow_right: Voir formation DAPI “**Accompagnement méthodo des étudiants pour soutenir leur réussite**“. 
1. Communautés de pratiques :arrow_right: CdP utilisation de l'IA générative en cours de lancement
1. Formation par la simulation :arrow_right: Pedagotalks sur ce thème cette année
1. Changer l'état d'esprit des EC sur la pédagogie :arrow_right: Lien avec la reconnaissance du travail pédagogique ? *Scholarship of Teaching and Learning*. Depuis quelques années, un peu plus facile de publier dans des revues “standard” des travaux en lien avec la pédagogie. En interne Idex Formation ; congé projet pédagogique. Projet création d'un Lab SoTL...
1. Accompagnement pédagogique :arrow_right: Complexe à mettre en place de l'accompagnement de proximité: gestion du temps et de la charge de travail des ingé péda. dans les composantes (plutôt y mettre des techniciens-ASI formés que des ingénieur·es). Modèle en cours d'élaboration et de test avec H3S

---
# :four: Thématiques de la DAPI en cours (au 6/12/24) (fm)

- Usages pédagogiques (notamment évaluatifs) de l'IA générative
- Simulation et improvisation pour la formation professionnelle
- Systèmes favorisant les démarches d'apprentissage collaboratives
- **Transition écologique pour un développement soutenable (TEDS)**
- Salles ambiantes pour la formation et la recherche
- **(In)formation sur la santé mentale (des enseignants/étudiants)**
- *Scholarship of Teaching and Learning* (SoTL)
- Open Labs, Learning Labs, Learning spaces
- Méthodologie de travail universitaire (cf. FlexiTLV)

---
# :four: Publication en exemple : Innovating Pedagogy (phd)

- “**Innovating pedagogy**” est une [publication](https://www.open.ac.uk/blogs/innovating/) annuelle de l'OUUK (*Open University UK*), dont c'est le 12e numéro, dont l'objet est de **prédire** les utilisations pédagogiques les plus probables de l'année à venir
- Chaque numéro contient une dizaine d'usages pressentis

---
# :four: Les thèmes du numéro 2024 (pour info) (phd)

- Les mondes imaginaires
- Pédagogie de la paix
- Pédagogie de l'action climatique
- Apprendre par la conversation avec l'IA générative
- Parler d'éthique de l'IA avec des jeunes
- L'écriture multimodale assistée par l'IA
- Les manuels intelligents
- Évaluation par la réalité étendue
- Langage immersif et culture
- L'exploration incarnée de modèles scientifiques

---
# :four: Plan de chaque contribution (inspiré d'Innov. Peda.) (env. 4 p.) (phd)

- Introduction : Définitions, utilisations et intérêt du thème dans l'ESR
- Quelques exemples issus de la recherche
- Effets relevés dans la recherche
- Défis, barrières et limites
- Exemples d'implantation possibles (dans le contexte UGA)
- Conclusion
- Références
- Ressources (accessibles sur le web)

---
# :five: Suite du travail en groupe (phd)

## Retour aux points 3 et 4 du Brainstorming
1. Ajouter un ou des point.s, en relation avec les ajouts thématiques
3. - **45 min** Faire des groupes aléatoires et interdisciplinaires. 
    - Examen de tous les sujets (du groupe, écrits en 1.) en prenant en compte les rappels du 2., regroupement et solidification :arrow_right: Arriver à une liste d'une quinzaine de sujets max. 
    - Affichage des sujets et priorisation 

4. **30 min** Répartition d'un sujet par binôme et réflexion sur le plan et avancement du projet, possibles collaborateur.es

---
# :five: Échéancier du travail (ph)

- **Vacances de décembre 2024** : Accord final sur une liste de thèmes à traiter et contributeur.es
- **Mars 2025** : Livraison du chapitre pour relectures croisées
- **Avril 2025** : Organiser une réunion intermédiaire (CS complet ou par thèmes) pour faire un point sur les doc. et sur le règlement intérieur du CS
- **Mai 2025** : Publication finale



---
# :six: Point sur le fonctionnement du CS (phd)

- **Info** : À ce jour, Sébastien Caudron a préféré stopper sa participation au CS
- Volonté de tous.tes de continuer dans le CS ? Aux mêmes conditions de travail que précédemment ?
- Point politique : élargissement aux établissements-composantes : ENSAG, Sciences Po, INP ; -> CNP (DAPI + Perform) va devenir une unité de services avec un CS.
- Élaboration d'un règlement plus précis du CS (sera soumis courant 2025 pour l'US)
- Principes règlementaires pour intégrer de nouveaux membres (pour remplacer les sortants et aussi étoffer le CS) ? Cooptation ? Vote ?
- Changement de chargé de mission à l'horizon mi-2026 avec tuilage

---
# :seven: Présentation rapide de l'Unité de Services CNP de l'UGA (FM)

<!-- _class: t-70 --> 

1. Avantage
    - meilleure mutualisation, collaboration avec l'IDEX
    - création Lab SoTL
    - soutien/pérennisation plateforme locale
1. Process
    - Dans un premier temps : fusion de Perform et DAPI dans le CNP (à voir l'élargissement à Sciences Po et ENSAG)
        - Validation en CSA et CA en juin-juillet
    - Regroupement des équipes au CLV
    - Opérationnelle à la rentrée 2025
    - Élargissement du périmètre du chargé de mission
