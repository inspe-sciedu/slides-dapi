---
marp: true
paginate: true
autoscale: true
theme: uga
---
<!-- _paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# Installation du Conseil scientifique de la DAPI

28 juin 2023

---
<!-- footer: Conseil scientifique DAPI 1re réunion | 28/06/23 | CC:BY-NC-SA -->
<!-- paginate: true -->

# Partie I

## Présentation du contexte

---

# Mot de David Déchenaud

- VP Formation UGA

---

# Présentation des membres (1/2)

-  Tour de table, chacun·e se présente et partage aux collègues :
  - une anecdote **vécue** (drôle, ou triste, ou mystérieuse, ou inspirante...) sur la pédagogie ou sur l'usage du numérique en contexte universitaire, commençant  par :
  - “Dans un cours de *X*, je me souviens que *Y*...” (pour une anecdote qui ne risque plus de se produire) 
  - “Dans un cours de *X*, je ne suis pas arrivé  à *Y* / j'ai fini par comprendre *Y* /  il s'est passé *Y* etc.


---
# Présentation (2/2)

- Anecdotes variées, sur la pédagogie universitaire, l'enseignement à distance, les classes inversées, les QCM, les serveurs Moodle, la prise de notes, etc.

---

# Le conseil scientifique de la DAPI

- Le CS de la DAPI est formé de chercheurs travaillant dans le domaine de la pédagogie universitaire appliquée à leur discipline d'enseignement et dans les champs connexes (sciences de l'éducation, psychologie de l'éducation).
- Se réunit une fois par an et aide la DAPI à définir des orientations et **donner son avis étayé pour le développement d'activités de formation et de développement professionnel**, dans les champs de la pédagogie universitaire et de l'intégration du numérique dans l'enseignement universitaire.

---

# Durée du mandat

- Ses membres sont nommés par la Commission de la formation et de la vie universitaire (CFVU) sur proposition conjointe de la Vice-présidence formation et de la DAPI, pour une durée de **4** ans synchronisée avec la durée d'un mandat présidentiel, le conseil est donc renouvelé à chaque début de mandat.
- :warning: La gouvernance de l'UGA étant renouvelée en janvier 2024, le rayon d'action de ce CS ne dépassera pas cette date, car on ne peut prédire si le CS sera renouvelé par l'équipe suivante.

---

# Rôles du conseil scientifique de la DAPI (1/2)

- Les rôles principaux du conseil scientifique de la DAPI sont les suivants : 
  - de produire **un livre blanc** à chaque début de mandat présidentiel pour alimenter le plan stratégique de la nouvelle équipe ; :warning: le mandat actuel du CS ne permettra pas son écriture
  - de donner, en cours de mandat et à un rythme annuel, des pistes de travail prospectives et des orientations dans le domaine de la pédagogie universitaire et de l'intégration du numérique ;

---

# Rôles du conseil scientifique de la DAPI (2/2)

- de donner ponctuellement des avis évaluatifs sur un projet pédagogique impliquant la DAPI, avant sa mise en œuvre, par exemple, mais pas exclusivement, il peut être mobilisé dans l'évaluation et la conception des projets Idex Formation) ;
- de donner un avis scientifique à propos des informations sur les pratiques pédagogiques au sein de l'UGA qui seront remontées de l'Observatoire de la pédagogie (en cours de mise en place) ;
- de participer annuellement à un atelier thématique d'une demi-journée où certaines pistes de travail qui viennent d'être définies seront exposées et discutées.

---

# Modalités de réunion

- Le conseil scientifique de la DAPI se réunit au moins une fois par an, sur une thématique définie à l'avance. Cette réunion est l'occasion de la mise en place de l'atelier thématique.
- Il sera important que les “livrables“ du CS soient présentés aux ingénieur·es de la DAPI pour discussion sur leur contenu et leur possible implémentation

---

# Le flux de décision à l'UGA

![](https://kroki.io/graphviz/svg/eNq1VcFu2zAMvesrNPuyAc5h58CHNG6yAHVnIEi2Iu1BsVhHmCy5khysGPpB-Y782Ci7cdo42w5dBSSRSIqPfCKZwrBqQ6f0F7nXyilWQhwwKcEEQxJKtgYZB6M812XFCgUlKBeQ0FZSKLBxpeWj36Gpt9C14rEztT8bpn5YqOLgMzoiRGkOdHUKEdkNqyBe65-RdY8SYuNdAL_DK-P56hl-rJUFIanNBcKLe_FQQ3BHkoM-EQZyp_EbpdmXTj7eMFPsdxSRS2Gt0IrOlNJb5vw22-84K3TReItoix9YLQUPEH-SvnYPtaHJKJudMU2Xnek0oRNtygbhjGXSxbbMXhj27G6uDnaZ2e-s4J72vrfpfHUqC32Mw7ClOtdSm9g6ALmWNUSn5_Zyc9czHi6uD7ALywqwVNXlfmc8Q_bwVoHSrsdXRFvXJXBRl642D7UWFiJ6jOJE5UO9Srsnxi0d0EzznufXqOdh_orytUsqY1yCu1WpUDxl1a36pnUuWfX_MS-6irjQwgkw1hfh9hxzb8YadYU6qrAt86akLMWHa4Df4d26V5uD2WJfvAOGX-F8cT277LD8od9S4R_cNz7C5PLYmrAFqavKx0ux3AZYbB85hkBnyeX3aKGEgw-fDpngkJC9djnTUneE2HpdNDM0wxlKcYX9QdqIT64Oj8ZnNW2inNkN8FaSyxptzPOE9ZI2N-wbiVX20Pn0w3bVi7RRAS_6qvYaTs7BIMHPzRUhT8e8Rrx878zaPGbXWXvz1X_BP3LH6IQS1hms-_s35T9JB4N0iRRM50NPgF9ehryQRp4Qz854slwMSUPWGA3Hc6_xIxt_m5ncEPhEfgPYyYFB)

---

# Présentation des services de la DAPI

![w:800px](images/DAPI_infographie.png)

---
# Suivre l'activité de la DAPI
- pour s'abonner à la liste de diffusion DAPI (1 mail par mois) : <https://listes.univ-grenoble-alpes.fr/sympa/subscribe/dapi-infos>
- [Pages intranet DAPI](https://intranet.univ-grenoble-alpes.fr/formation-et-vie-etudiante/pedagogie-et-innovation/)

---

# La charge de mission DAPI

1. Former et animer la communauté universitaire

2. Accompagner, soutenir et coordonner les projets d’amélioration de la qualité pédagogique

3. Valorisation

4. Veille et expérimentations

5. Appui à la stratégie

##### 

---

# Partie II

Description du travail

---

# Rôles du CS DAPI d'ici à début 2024

- Donner des idées de quelques éléments du livre blanc (contenu, structure)
- Donner des idées de thèmes pour la 1/2 journée thématique de 2024
- Expertise sur les congés pour projets pédagogiques ; avis ponctuels, sollicités par les IE de la DAPI (ventilés par le chargé de mission ou le directeur DAPI);
- Susciter de possibles pistes de recherche pouvant être menées dans les composantes de l'UGA

---

# 1. Le livre blanc

- À l'image des rapports “Innovating pedagogy“ (<https://www.open.ac.uk/blogs/innovating/>), pourrait présenter quelques dispositifs, outils, méthodes pédagogiques pouvant être promues et mises en place à l'UGA
- Courts exposés thématiques d'env. 4 p. pour chaque thème
  - présenter la méthode/l'outil, etc.,  
  - des preuves de son efficacité dans l'ens. sup. (bénéfices/défis)
  - des pistes d'utilisation concrètes dans l'ESR, et notamment à l'UGA
- Possibilité d'héberger le livre blanc sur un serveur UGA (péda. univ), sous licence CC:BY-NC-SA

---
# 1.bis État des pratiques

Il sera intéressant de coupler le livre blanc avec un état des pratiques au niveau de l'UGA, issus de l'Observatoire des pratiques, en cours de création.

---

# 2. La demi-journée thématique (2024)

- Non traité, à proposer ultérieurement

---

# 3. Avis sur CPP/ situations diverses

- Au fil de l'eau, avec le filtre du dir. DAPI et/ou du chargé de mission, les membres du CS pourront être saisis pour donner leur avis, par exemple sur des projets de CPP (Congés pour projets pédagogiques), sur telle ou telle décision politique, etc.
- Les membres du CS peuvent également faire la publicité de ces CPP dans leurs composantes, puisqu'ils sont sous-sollicités

---

# 4. DAPI -> CS ?

- En quoi l'UGA et la DAPI pourrait aider les membres du CS dans leurs thèmes de recherche ?
- Demandes régulières des enseignants à la DAPI sur tel ou tel thème ; exemples :
  - faire des cours et demander aux étudiants de prendre des notes manuscrites
- Les membres peuvent aussi proposer des thèmes de recherche, et la DAPI aidera à trouver des terrains

---
# 5. Liens avec CFVU

Il apparaît également possible que des membres du CS puissent être amenés à présenter des informations de leur champ de compétences au CFVU de l'UGA

---

# Partie III.

#  Réflexion

---

# Questions de réflexion

- Dans les 4 prochaines années, qu'est-ce qu'on voudrait voir avancer à l'UGA   
  1/ en pédagogie universitaire ? 2/ en usages du numérique  ? et comment s'y prendre ? 
- **(15 min) Individuellement**, mentionner sur des papiers-notes repositionnables des idées sur ces 2 questions (1 idée par question) 
- **(20 min)** **Par petits groupes de 2-3** (si possible pluri-disciplinaires), tri des idées.
- **(15 min)** Mise en collectif et discussion

---

# Feuille de route

- Pour chaque thème mis au jour : 
  - qui est expert·e de ce thème ?
  - niveau de priorité pour le livre blanc ; pour la première 1/2 journée thématique ?
  - Se répartir le travail sur les thèmes, extraire env. 4-6 thèmes pour commencer à rédiger des chapitres du livre blanc

---

# Échéancier

- **Décembre 2024** : 1er jet du livre blanc
:warning: Difficile de prévoir d'autres points pour l'instant...

# 