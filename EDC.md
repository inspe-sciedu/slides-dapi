# Modèle d'écriture des parcours de mention accréditée en activités et en compétences à l'Université Grenoble Alpes

## Cadre réglementaire

Depuis 2018, le cadre réglementaire nous impose désormais d'avoir nos diplômes écrits non seulement en se structurant en unités d'enseignement et en ECTS mais aussi en référence aux blocs de compétences qui sont inscrits dans les fiches RNCP de licence et de master

- [Arrêté du 30 juillet 2018 modifiant l&#39;arrêté du 22 janvier 2014 fixant le cadre national des formations conduisant à la délivrance des diplômes nationaux de licence, de licence professionnelle et de master](https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000037291140)

- [Arrêté du 30 juillet 2018 relatif au diplôme national de licence ](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000037291166/)

- [Article 17 - Arrêté du 6 décembre 2019 portant réforme de la licence professionnelle ](https://www.legifrance.gouv.fr/jorf/article_jo/JORFARTI000039481599) 

## Cadre national des fiches RNCP

La délivrance des diplômes accrédités est associée à l'inscription de droit au Répertoire National des certifications professionnelles (RNCP).

Les fiches RNCP  sont éditées par France Compétences.

Pour le titre d'ingénieur diplômé, la validation par la Commission des titres d’ingénieur (CTI)  doit être préalablement obtenue.

Les documents de référence sont en fonction du contexte:

- [Documentation de France Compétences](https://www.francecompetences.fr/app/uploads/2020/01/Note-relative-aux-r%C3%A9f%C3%A9rentiels-dactivit%C3%A9s-de-comp%C3%A9tences-et-d%C3%A9valuation.pdf)

- [Documentation de la CTI](https://www.cti-commission.fr/wp-content/uploads/2022/03/Fiche_thematique_RNCP_2022-03-04.pdf)

## Stratégie locale

La démarche compétences s'exprime à un double niveau :

- celui des mentions de formations, avec une obligation nationale d'écrire nos formations en compétences sur la base des référentiels nationaux qui sont assez génériques, et de l’affichage de la contribution des unités d’enseignement aux blocs constitutifs des mentions.

- celui des parcours de formations, avec une ambition locale permettant une écriture plus détaillée en lien avec les compétences spécifiques acquises par nos étudiants dans le cadre de parcours de formations qui sont souvent singuliers à l'échelle de chaque université.

Les équipes de la vice-présidence formation souhaitent poursuivre le travail dans ce double niveau. Le second est naturellement plus ambitieux que le premier, mais a aussi plus de sens pour les équipes pédagogiques, les étudiants ainsi que les milieux socio-économiques.

## Objectifs de l'écriture en compétences

Décrire et lister les activités clés, contribuant à l’exercice d’un métier, réalisées par les étudiants pendant un parcours de formation permet :

1. de répondre aux évolutions du cadre réglementaire

2. de faciliter la rédaction des référentiels pour dépôts au RSCH et RNCP

3. une meilleur employabilité et insertion professionelle
   
   - par meilleure lisibilité et visibilité de la formation pour les milieux socio-économiques et les étudiants.
   
   - aider les étudiants à faire le lien entre enseignements et futures activités professionnelles (motivation et engagement par une meilleure compréhension de l'articulation entre situation en formation et  situation professionelle)

4. la personnalisation des parcours
   
   . des changements d'orientation possibles par une transférabilité des compétences
   
   . adaptation aux besoins spécifiques (sportifs, artistes de haut niveau )

5. de favoriser la formation tout au long de la vie (FTLV)
   
   - en faisant des ponts entre formation initiale et formation continue
     
     - Alternance
     
     - Apprentissage
   
   - par la modularisation des formations
     
     - VAE en blocs
     
     - inscription aux blocs de compétences

6. une meilleure professionalisation des étudiants (par exemple le Bachelor Universitaire de Technologie)

## Rendus attendus

1. Élaboration du référentiel de compétences de la formation 
   
   - Définition du référentiel de compétences visées par la formation si la fiche RNCP doit être adaptée aux spécificités de la formation
   - Explicitation des acquis d’apprentissages qui en découlent
   - Alignement sur les blocs RNCP si le référentiel est une adaptation locale de la fiche RNCP.
   
   ![](images/2023-06-19-10-15-24-image.png)

2. Lien avec les maquettes existantes ou nouvelle maquette
   
   1. Identifier les enseignements qui couvrent au mieux les acquis d’apprentissage (Matrice)
      ![](images/2023-06-19-10-21-40-image.png)
   2. Regrouper les enseignements mobilisant les compétences acquises dans les différents enseignements pour correspondre aux blocs de copmpétences (si nécessaire)

3. Définition des modalités d’évaluation et de certification des compétences
   
   - Élaboration de situation d'apprentissage et d'évaluation (SAÉ) participant à l’évaluation du bloc
   - Évolution vers des blocs non compensables
   - SAÉ certifiant les compétences

4. Remonté et suivi du processus pour validation institutionelle
   Les données renseignées sont communiquées aux services administratifs et aux instances
   intentionnelles compétentes. Elles pourront être soumises à validation lors des campagnes annuelles des remontés des RDE et MCCC.

|                                                                                        |                                                                                                                     |
| -------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------- |
| Mention : Nom de la mention / <br/>Parcours : Nom du parcours                          | Suivi du document par :<br/>Nom du ou des responsables administratifs en charge de l’accompagnement et /ou du suivi |
| Responsable pédagogique : <br> Nom du responsable \| Fonction ou titre<br>             | Document visé et/ou validé par : <br/>Nom Prénom  Fonction \| Nom Prénom Fonction                                   |
| Référent du dossier au sein de la formation :<br/>Nom de référent \| Fonction ou titre | Date de visée ou de validation du document :<br/> JJ-MM-AAAA                                                        |
| Objet du document : Fiche de synthèse EDAC – UGA 2023                                  | Cadre Réservé à l’administration                                                                                    |

## Adaptabilité du modèle

En fonction des objectifs de l'équipe pédagogie et pour tenir compte des différents contextes de l'établissement, le niveau de compléxité et les rendus attendus ne seront pas les mêmes .

|                                           | 1. référentiel de compétences | 2.1 acquis d’apprentissage | 2.2 Blocs | 3. Situations d'évaluation | 4. Validation |
| ----------------------------------------- |:-----------------------------:|:--------------------------:|:---------:|:--------------------------:|:-------------:|
| dépôts au RSCH et RNCP                    | X                             |                            |           |                            |               |
| cadre réglementaire                       | X                             | X                          |           |                            | X             |
| employabilité et insertion professionelle | X                             | X                          |           |                            | X             |
| personnalisation des parcours             | X                             | X                          | X         |                            | X             |
| formation tout au long de la vie          | X                             | X                          | X         | X                          | X             |
| besoins professionalisation               | X                             | X                          | X         | X                          | X             |

## Accompagnement

Chaque équipe pédagogique peut travailler en autonomie ou demander à être accompagné par les structures identifiés au sein de l'établissement comme étant en charge de la formation et de l’accompagnement à la Démarche compétence et / ou à l’approche par les compétences ( Perform - INP ou  DAPI)  

## Le processus

La rédaction du référentiel de compétences se déroule en 5 étapes:

1. Identifier et décrire les situations de formation

2. Recenser les activités clés au regard des situations

3. Décrire une à plusieurs activités clés liées aux situations

4. Décrire et rédiger les compétences associées à l’activité

5. Décrire les ressources nécéssaires et les les modalités d’évaluations

Chaque étape se fait sous forme d'entretien, d'atelier ou d'échange entre les participants. Ce processus dure un minimum de 3 à 4 demi-journées plus ou moins espacées dans le temps. Le processus est défini plus précisément lors du premier entretien avec le responsable du parcours ou de la mention.

## Champs d'application

Le modèle éprouvé d'écriture des parcours de mention accréditée en activités et en compétences de l'Université Grenoble Alpes est issu des différentes expériences et des accompagnements menés depuis 2018 notament dans le cadre du plan d’investissement et d’avenir NCU-FlexiTLV, ainsi que de l’analyse et du suivi du cadre réglementaire.

Le modèle d’écriture en activité et en compétences des parcours de mention accréditée s’applique par défaut à l’établissement ainsi qu’à l’ensemble de ses composantes. 
Cependant l’offre de formation accréditée au sein des composantes de l’établissement dont les parcours de mention doit répondre à un cadrage défini par des instances nationales (ex : Commission des titres d’ingénieur) ou prépare à des métiers réglementés déjà soumis à des référentiels d’activités et de compétences de branche ou faisant l’objet d’un arrêté d’habilitation, peut proposer des modèles alternatifs permettant d'obtenir des rendus respectant le cadre national.
