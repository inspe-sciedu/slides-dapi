# Liste des présentations DAPI

- Résultats de la 2e réunion du CS DAPI, 17/01/25 [[pdf](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-dapi/resultats-cs-2.pdf)][[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-dapi/resultats-cs-2.html)]

- 2e réunion du Conseil scientifique de la DAPI, le 6/12/24 [[pdf](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-dapi/CS-2.pdf)][[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-dapi/CS-2.html)]

- Analyse de la 1re réunion du CS DAPI (Dessus & Ménard, présentation le 13/11/23 en réunion DAPI) [[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-dapi/Analyse-S1-CS-DAPI.pdf)][[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-dapi/Analyse-S1-CS-DAPI.html)]  

- 1re réunion du Conseil scientifique de la DAPI, le 28/06/23 [[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-dapi/CS.pdf[HTML](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-dapi/CS.html))]

- Enseignement à distance et e-learning, présentation le 2/02/23 en CFVU-UGA
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-dapi/pres-ead-final-cfvu.pdf)]
[[web](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-dapi/pres-ead-final-cfvu.pdf)]

- Enseignement à distance et e-learning, présentées le 12/01/23 en commission pédagogique CFVU-UGA 
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-dapi/pres-ead.pdf)]
[[web](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-dapi/pres-ead.html)]

- L'usage des méta-analyses et des revues systématiques en pédagogie universitaire (Dessus & Cojean, présentation le 14/03/23 à la DAPI)
[[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-dapi/pres-meta.pdf)]
[[web](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-dapi/pres-meta.html)]
