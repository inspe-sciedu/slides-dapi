---
marp: true
paginate: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)


# Liste des présentations DAPI

Philippe Dessus & Fabrice Ménard, Univ. Grenoble Alpes

---
<!-- paginate: true -->

- Enseignement à distance et e-learning, présentation le 2/02/23 en CFVU-UGA
[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-dapi/pres-ead-final-cfvu.pdf)
[web](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-dapi/pres-ead-final-cfvu.pdf)

- Enseignement à distance et e-learning, présentées le 12/01/23 en commission pédagogique CFVU-UGA 
[PDF](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-dapi/pres-ead.pdf)
[web](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/slides-dapi/pres-ead.html)

