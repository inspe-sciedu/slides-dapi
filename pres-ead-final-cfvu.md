---
marp: false
paginate: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)


# Enseignement à distance & e-learning

Philippe Dessus & Fabrice Ménard
Univ. Grenoble Alpes

CFVU du 2/02/23

---
<!-- paginate: true -->
<!-- footer: Ph. Dessus & F. Ménard | EAD-CFVU UGA | CC:BY-NC-SA | 2/02/23 -->

## 0. Faire de l'e-learning ?

Amène à régler tout ou partie de ces questions, souvent en comparaison avec des situations en présence :

- quel est le **but de l'apprentissage** (préparer aux examens ? apprendre des compétences ?)
- quelle est ma **pédagogie** ? 
- quel est le rôle de la **technologie** ? (remplacer ou augmenter l'enseignant)
- comment est-ce que j'**évalue** ? (à l'identique ? différemment ?)
- quels **biais** est-ce que je crée ? (inégalités dues à la technologie elle-même ou au traitement des données recueillies)

---
## 1.1 Des problèmes liés à l'enseignant influent sur la “rétention“ des étudiants 

<!-- _class: t-90 -->
- **Conception du cours défectueuse** : manque d'organisation dans le cours, structuration illogique, difficulté pour localiser les ressources, éléments de cours intéressants et non pertinents :arrow_right: **Structurer le cours**
- **Engagement et sens d'appartenance réduits** : absence d'indices verbaux et visuels, étudiants isolés et mal guidés, présence sociale faible, interactions faible :arrow_right: **Favoriser les interactions**
- **Apprentissage faiblement guidé** : Faible présence de l'enseignant dans le guidage de l'apprentissage, manque d'aide pour aider à la compréhension, interactions enseignant-étudiants de faible qualité ; devoirs avec faibles interactions :arrow_right: **Donner des consignes et des rétroactions détaillées**

(Salim Muljana & Luo 2019)

---
## 1.2 Cyber-harcèlement et autres problèmes

- Comportements inapropriés en visioconférence
- Production d'un document avec la cellule signalement VSS / Discirmination

---
## 2.1 Bilan de l'e-learning “urgent et forcé” : une MOOCification ?

- **Avantage** : a permis la sensibilisation des enseignants et étudiants aux outils d'e-learning, certains ont envie de creuser les potentialités de l'e-learning
- **Inconvénients** : fuite d'informations due au recours massif d'outils propriétaires (Human Rights Watch 2022) ; inégalités d'accès au matériel ; construction et passation d'évaluations spécifique et délicate ; création de ressources de faible qualité, donc peu pérennes

---
## 2.1 Enquête UGA sur l'impact de la situation de pandémie

- Utilisation des plateformes plus fréquente :arrow_right: 61%
- Découverte de nouvelles fonctionnalités :arrow_right: 42%
- Changement des pratiques pédagogiques :arrow_right: 61%

---
## 3. Un parcours de ressources sur la conception et mise en œuvre

Ressources libres et gratuites sous BY-NC-SA, donc réutilisables et mixables

https://link.infini.fr/cfvu-uga-e-learning

![w:300](images/parcours-e-learn.jpg)

---
## 4. Exemple, l'évaluation des étudiants à distance

> “*Les élèves peuvent, avec difficulté, s’échapper des effets d’un mauvais enseignement ; ils ne peuvent (s’ils veulent réussir dans un cours) échapper aux effets d’une mauvaise évaluation.*” (McDonald et al. 2000)

- Les QCM, seuls moyens d'évaluation ? Essayer de concevoir des tâches évaluatives authentiques et formatives (voir parcours)
- Attention au *proctoring* (surveillance à distance)
- Effets des outils de génération de textes (ChatGPT) ?

---
## 5.1 Discussion : questions éthiques

- **Côté étudiants** : fraude, plagiat, utilisation de logiciels de création automatique de dissertations (*chatGPT*)
- **Côté enseignants** : surveillance, non respect des données personnelles, promotion d'évaluations jetables ou authentiques, mise à disposition de communs ?
- **Votre avis ?**

---
## 5.2 Rôle de la DAPI ?

- Guidage pour la prise en main d'outils vs. guidage pédagogique ?
- Aide à l'évaluation du temps de travail étudiant, qui a tendance à augmenter dans l'e-learning


--- 
## 6. Références

<!-- _class: t-60 -->

- Human Rights Watch. (2022). *“How Dare They Peep into My Private Life?” Children’s Rights Violations by Governments That Endorsed Online Learning During the Covid-19 Pandemic*. New York: Human Rights Watch.
- McDonald, R., Boud, D., Francis, J., & Gonczi, A. (2000). *New perspectives on assessment. Vol. 4*. Paris: UNESCO.
- Salim Muljana, P., & Luo, T. (2019). Factors Contributing to Student Retention in Online Learning and Recommended Strategies for Improvement: A Systematic Literature Review. *Journal of Information Technology Education: Researc*h, *18*, 019-057. doi: 10.28945/4182