---
marp: true
paginate: true
autoscale: true
theme: uga
---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)


# Enseignement à distance & e-learning

Philippe Dessus & Fabrice Ménard
Univ. Grenoble Alpes

Commission pédagogique du 12/01/23

---
<!-- paginate: true -->
<!-- footer: Ph. Dessus & F. Ménard | Commission pédagogique UGA | CC:BY-NC-SA | 12/01/23 -->

## 0. Faire de l'e-learning ?

Amène à régler tout ou partie de ces questions, souvent en comparaison avec des situations en présence :

- quel est le **but de l'apprentissage** (préparer aux examens ? apprendre des compétences ?)
- quelle est ma **pédagogie** ? 
- quel est le rôle de la **technologie** ? (remplacer ou augmenter l'enseignant)
- comment est-ce que j'**évalue** ? (à l'identique ? différemment ?)
- quels **biais** est-ce que je crée ? (inégalités dues à la technologie elle-même ou au traitement des données recueillies)

---

## 0. Quelques manières d'aborder l'e-learning à l'UGA

1. Analyser les différences distance vs. présence
2. Analyser la “présence” de l'enseignant et des étudiants
3. Le bilan de la mise en place d'un enseignement en ligne d'urgence
4. Conseiller sur des “recettes” pour enseigner ou apprendre en ligne
5. Conseiller sur l'e-learning en utilisant des données probantes
6. Discussion
7. Références

---
## 1. La comparaison distance vs. présence

- Une récente méga-analyse (Martin et al. 2022) montre un effet faible, bien que significativement supérieur, de l'e-learning comparé à l'enseignement en présence pour le volet cognitif (*g* = .2) ; aucune différence pour les volets affectifs ou comportementaux.

---
## 2.1 Analyser la présence en ligne

- Notion complexe, avec de multiples définitions et dimensions
- La distance de transaction de Moore (1993), voir Dessus *et al.* (1997)
- Les communautés d'enquête (Garrison *et al.* 2010) avec différents types de présence (cognitive, sociale, d'enseignement)

---

![bg w:700](images/dtransaction.jpg)
![bg w:600](images/coi.jpg)

---
## 2.2 Des problèmes liés à l'enseignant influent sur la “rétention“ des étudiants 

<!-- _class: t-90 -->
- **Conception du cours défectueuse** : manque d'organisation dans le cours, structuration illogique, difficulté pour localiser les ressources, éléments de cours intéressants et non pertinents :arrow_right: **Structurer le cours**
- **Engagement et sens d'appartenance réduits** : absence d'indices verbaux et visuels, étudiants isolés et mal guidés, présence sociale faible, interactions faible :arrow_right: **Favoriser les interactions**
- **Apprentissage faiblement guidé** : Faible présence de l'enseignant dans le guidage de l'apprentissage, manque d'aide pour aider à la compréhension, interactions enseignant-étudiants de faible qualité ; devoirs avec faibles interactions :arrow_right: **Donner des consignes et des rétroactions détaillées**

(Salim Muljana & Luo 2019)

---
## 3.1 Bilan de l'e-learning “urgent et forcé” : une MOOCification ?

- **Avantage** : a permis la sensibilisation des enseignants et étudiants aux outils d'e-learning, certains ont envie de creuser les potentialités de l'e-learning (Charroud et al. 2020)
- **Inconvénients** : fuite d'informations due au recours massif d'outils propriétaires (Human Rights Watch 2022) ; inégalités d'accès au matériel ; construction et passation d'évaluations spécifique et délicate ; création de ressources de faible qualité, donc peu pérennes


---
## 3.2 Une enquête à l'UGA

- Utilisation des plateformes plus fréquente :arrow_right: 61%
- Découverte de nouvelles fonctionnalités :arrow_right: 42%
- Changement des pratiques pédagogiques :arrow_right: 61%



---
## 4.1 Conseiller sur des “recettes”

- Il existe de très nombreux sites, et d'approches, de conseils en e-learning
- Les conseils peuvent reposer sur des éléments subjectifs, mal cernés, plutôt que sur des éléments réellement efficaces
- Mais **tout ne se chiffre pas**, et aider, orienter, les enseignants à optimiser leur travail en ligne/hors ligne est utile
- Conseils sur des “micro-activités“ réalisées collectivement et collaborativement, comme celles présentées dans ce [document](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/peda-univ/ens-en-ligne.html#briques-dactivites)
- Évaluer la charge de travail des étudiants : [Workload Estimator](https://cat.wfu.edu/resources/tools/estimator2/)

---
## 4.2 Un exemple, l'évaluation des étudiants à distance

> “*Les élèves peuvent, avec difficulté, s’échapper des effets d’un mauvais enseignement ; ils ne peuvent (s’ils veulent réussir dans un cours) échapper aux effets d’une mauvaise évaluation.*” (McDonald et al. 2000)

- Les QCM, seuls moyens d'évaluation ? Essayer de concevoir des tâches évaluatives authentiques et formatives (voir ce [document](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/peda-univ/evaluation-online.html))
- Attention au *proctoring* (surveillance à distance) : biais et atteinte à la vie privée (plus d'informations dans ce [document](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/peda-univ/surveillance-etu.html))

---
## 4.3 Cyber-harcèlement et autres prob.

- Comportements inapropriés en visioconférence
- Production d'un document avec la cellule signalement VSS / Discirmination

---
## 5.1 Fonder les conseils sur des données probantes

- Pour la plupart des facettes de l'e-learning, il existe des méta-analyses (synthèses de recherches) analysant ses effets et pointant les pratiques efficaces
- Une recension raisonnée de ces études peut être utile pour mettre en œuvre des séances de formation

- Plus d'informations dans ce [document](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/peda-univ/distance-reco.html)

---
## 5.2 Quelques recommandations issues de données probantes

- Les interactions enseignant-étudiants

    - les étudiants s’entre-aident dans la mise en œuvre de procédures
    - les étudiants s’entre-évaluent leurs productions

- Méthodes pédagogiques

    - les étudiants sont confrontés à une simulation, e.g., avec patients, dans le domaine médical
    - les étudiants génèrent des questions pour produire un résumé à propos du cours
    - les étudiants sont confrontés à un guidage ou diagnostic personnalisé


---
## 6.1 Discussion : questions éthiques

- **Côté étudiants** : fraude, plagiat, utilisation de logiciels de création automatique de dissertations (*chatGPT*)
- **Côté enseignants** : surveillance, non respect des données personnelles, promotion d'évaluations jetables ou authentiques, mise à disposition de communs ?
- **Votre avis ?**

---
## 6.2 Rôle de la DAPI ?

- Guidage pour la prise en main d'outils vs. guidage pédagogique ?
- Aide à l'évaluation du temps de travail étudiant, qui a tendance à augmenter dans l'e-learning

---
## 6.3 Discussion

- Banque de documents et aide de la DAPI
- Stratégie de communication à l'ensemble du personnel ?
- Refondre certaines démarches d'enseignement (interactions, évaluation) pour les adapter à l'e-learning ?
- Peser les risques de la surveillance et de la fuite des données personnelles (*e.g.*, dans les visioconférences, dans le *proctoring*) ; être attentifs aux biais induits par l'utilisation de technologies

---
## 6.4 Documents-ressources DAPI sur l'e-learning

### Documentation technique 

- https://intranet.univ-grenoble-alpes.fr/les-tutoriels/
- [Création de pages Sphinx sur le GitLab Gricad](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/reflexpro/)

### Conseils

- [Enseignement en ligne : principes et activités](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/peda-univ/ens-en-ligne.html)
- [Evaluation formative des étudiants à distance](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/peda-univ/evaluation-online.html)
- [La surveillance numérique des étudiants lors des examens](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/peda-univ/surveillance-etu.html)
- [Enseignement hybride ou à distance : des recommandations](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/peda-univ/distance-reco.html)



--- 
## 7. Références

<!-- _class: t-60 -->

- Charroud, C., Dessus, P., & Osete, L. (2020). Confinement et pratiques évaluatives : une MOOCification urgente et forcée ? *Évaluer - Journal International de Recherche en Éducation et Formation (e-JIREF)*, *1*, hors-série, 53–58.
- Dessus, P., Lemaire, B., & Baillé, J. (1997). Études expérimentales sur l'enseignement à distance. *Sciences et Techniques Educatives*, *4*(2), 137–164. 
- Garrison, D. R., Anderson, T., & Archer, W. (2010). The first decade of the community of inquiry framework: A retrospective. *The Internet and Higher Education*, *13*(1-2), 5-9. 
- Human Rights Watch. (2022). *“How Dare They Peep into My Private Life?” Children’s Rights Violations by Governments That Endorsed Online Learning During the Covid-19 Pandemic*. New York: Human Rights Watch.
- Martin, F., Sun, T., Westine, C. D., & Ritzhaupt, A. D. (2022). Examining research on the impact of distance and online learning: A second-order meta-analysis study. *Educational Research Review*, *36*. doi: 10.1016/j.edurev.2022.100438
- McDonald, R., Boud, D., Francis, J., & Gonczi, A. (2000). *New perspectives on assessment. Vol. 4*. Paris: UNESCO.
- Moore, M. G. (1993). Theory of transactional distance. In D. Keegan (Ed.), *Theoretical Principles of Distance Education* (pp. 22–38). Routledge. 	
- Salim Muljana, P., & Luo, T. (2019). Factors Contributing to Student Retention in Online Learning and Recommended Strategies for Improvement: A Systematic Literature Review. *Journal of Information Technology Education: Researc*h, *18*, 019-057. doi: 10.28945/4182