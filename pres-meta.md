---
marp: true
paginate: true
autoscale: true
theme: uga

---
<!-- paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

## L'usage des méta-analyses et des revues systématiques en pédagogie universitaire
## Philippe Dessus* & Salomé Cojean** 
### *LaRAC & LIG, UGA  **LaRAC, UGA
### DAPI, 14 mars 2023

% https://onlinelibrary.wiley.com/loi/18911803/year/2022

---
<!-- paginate: true -->
<!-- footer: Dessus & Cojean | Méta-analyses & Revues systématiques - DAPI | CC:BY-NC-SA | 14/03/23 -->

# 0. Objet de la réunion

- Prendre connaissance de ce que sont les méta-analyses et les revues systématiques
- Réfléchir à leur intérêt pour la formation des enseignants du supérieur


---
# 1.1 Sources des preuves d'efficacité d'une démarche pédagogique

- nous-mêmes (notre propre opinion, avis)
- notre expérience passée
- nos étudiants
- nos pairs plus ou moins expérimentés
- les blogs, internet, réseaux sociaux
- les articles ou ouvrages scientifiques
- etc.

---
# 1.2 Toutes les preuves ne se valent pas

- La validité des différentes preuves dépendent plus ou moins
  - des opinions de celleux qui les émettent
  - de leurs possibles conflits d'intérêt
  - des méthodes utilisées pour les mettre au jour (échantillon plus ou moins grand, aléatorisation, rigueur, etc.)

- Démarches de recherche (assez) standardisées, et donc réplicables, pour obtenir des résultats comparables entre eux : les méta-analyses et les revues systématiques


---
# 2.1 Méta-analyses (1/2)

- Recherche compilant les résultats *quantitatifs* d'un ensemble d'études ayant le même but, dont les différentes étapes (sélection, extraction, analyse des biais, résultats) sont standardisées pour en permettre la réplication ou l'extension. 
- Résultat exprimé le plus souvent en **taille d'effet** (écart moyen standardisé entre les performances du groupe-contrôle et du groupe expérimental, c-à-d ramené à l'écart-type combiné), soit le *d* de Cohen. si *d* = 1, alors la moyenne des participants du groupe expérimental est à un écart-type de celle des participants du groupe-contrôle
- Une valeur de *d* = 0,2 est un effet faible, *d* = 0,5 un effet moyen et *d* = 0,8 un effet important
- Voir cette [page](https://rpsychologist.com/fr/cohend/) pour jouer avec les valeurs 
 
---
# 2.2 Méta-analyse (2/2)

 ![w:700](images/d-cohen.png)

---
# 2.3 Revues systématiques (1/2)

- Recherche compilant les résultats *qualitatifs* d'un ensemble d'études ayant le même but, dont les différentes étapes sont standardisées (recherche, sélection, résultats)
- Souvent décrites par le diagramme PRISMA (*Preferred Reporting Items for Systematic Reviews and Meta-Analyses*), qui permet de visualiser les étapes du processus de recherche et sélection des études analysées
- Utile pour avoir une vision précise d'une question de recherche

---
# 2.4 Revues systématiques (2/2)

[Source](http://www.prisma-statement.org/documents/PRISMA%20French%20Flow%20Diagram.pdf)


![w:500](images/prisma.png)

---
# 3.1 L'essor des méta-analyses et revues systématiques

- Les méta-analyses et revues systématiques sont (assez) aisées à comprendre, mais aussi à faire
- Il y a un essor de travaux de ce type en éducation (importées de la médecine)
- De nombreux auteurs, fondations et revues sont spécialisés dans les méta-analyses (MA) ou revues systématiques (RS)

---
# 3.2 Quelques exemples d'auteurs et de revues

Méta-analyses 
  - [Campbell Collaboration](https://www.campbellcollaboration.org/component/jak2filter/?Itemid=1352&issearch=1&isc=1&category_id=101&xf_4[0]=2&xf_8[0]=3&ordering=publishUp)
  - [Site de J. Hattie, visible learning](https://visible-learning.org/fr/) 
  - [Review of Educational Research](https://journals.sagepub.com/home/rer)
  - [Educational Research Review](https://www.sciencedirect.com/journal/educational-research-review)

Revues systématiques
  - voir le manuel de Zawacki-Richter *et al*. 2020

---
# 3.3 Le graal de la recherche ?

![w:700](images/ebmpyramid.jpg)

Voir Blunt (2022) pour une recension de ces pyramides. [Source](https://s4be.cochrane.org/blog/2014/04/29/the-evidence-based-medicine-pyramid/)


---
# 3.4 :warning: !! :warning: !! :warning: !! :warning: !!

- Ne s'intéresse qu'à des choses qu'**on peut mesurer** (et en éducation, tout ne se mesure pas aisément)
- **Déchets en entrée, déchets en sortie** : la qualité de la méta-analyse est celle des études qu'elle recense
- Il ne faut pas se contenter de la **taille d'effet globale** mais étudier les interactions avec d'autres variables
- Les **effets indésirables** d'une approche sont rarement rapportés dans les méta-analyses (Zhao 2017)

---
# 4.1 Notre document de synthèse : Recension de 70 effets de l'ens. à distance

[Enseignement à distance ou hybride : des recommandations](https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/peda-univ/distance-reco.html)

- Structure du document :  
  - Effets “standards” de l'enseignement
  - Interactions enseignant-étudiants et entre étudiants
  - Méthodes pédagogiques
  - Type de matériel présenté aux apprenants
  - Recommandations (en filtrant les effets *d* ou *g* > 0,7)

- ... et d'autres à venir ensuite !

---
# 4.2 Quelques recommandations (*d* > .7)

- Les interactions enseignant-étudiants

    - les étudiants s’entre-aident dans la mise en œuvre de procédures
    - les étudiants s’entre-évaluent leurs productions

- Méthodes pédagogiques

    - les étudiants sont confrontés à une simulation, *e.g*., avec patients, dans le domaine médical
    - les étudiants génèrent des questions pour produire un résumé à propos du cours
    - les étudiants sont confrontés à un guidage ou diagnostic personnalisé

---
# 4.3 Mots-clés pour chercher les MA et RS ?

MA
  - *meta-analysis* ; méta-analyse
  - *mega-analysis* (méta-analyse de méta-analyses) ; méga-analyse 

RS
  - *systematic review* ; revue systématique
  - *scoping review* ; revue exploratoire de la littérature


---
# 5. Discussion

- Les méta-analyses et les revues systématiques sont-elles utiles à la DAPI ? À quels moments des formations ? Pour quoi faire ?
- Comment les citer ? Les expliquer ?
- Compléter le document initial pour avoir une base de données au niveau universitaire ? En réaliser de nouveaux ?
- Se servir des catégories d'études pour structurer le travail de la DAPI ?

---
# Références

- Blunt, C. J. (2022). The Pyramid Schema: The Origins and Impact of Evidence Pyramids. Report of the London School of Economics. doi:10.13140/RG.2.2.16118.88643
- Zawacki-Richter, O., Kerres, M., Bedenlier, S., Bond, M., & Buntins, K. (Eds.). (2020). *Systematic Reviews in Educational Research. Methodology, Perspectives and Application*. Springer. 
- Zhao, Y. (2017). What works may hurt: Side effects in education. *Journal of Educational Change*, *18*(1), 1-19. https://doi.org/10.1007/s10833-016-9294-4 