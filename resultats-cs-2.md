---
marp: true
paginate: true
autoscale: true
theme: uga
---
<!-- _paginate: false -->
<!-- _class: invert -->
![bg left](assets/fondpageentete.jpg)

# Résultats du Conseil scientifique de la DAPI : 2e réunion
## DAPI, UGA

17 janvier 2025

<!-- footer: Résultats Conseil scientifique DAPI 2e réunion | 6/12/24 | CC:BY-NC-SA -->
<!-- paginate: true -->

---
# Introduction
- Ce document liste les décisions prises pendant la 2e réunion du CS de la DAPI, UGA, du 6 décembre dernier.
- Elles sont à (éventuellement) amender et valider par les membres

---
# :one: Liste des expertises des membres du CSI DAPI

Ci-après, par membre du CS, les 4 ou 5 thèmes d'expertise ou d'intérêt à développer dans le cadre de la pédagogie universitaire à l'UGA

---
# JCQ

- Interactivité avec ressources/supports ; Bases d'outils/matériels ; Tutoriels vidéos/textes/interactifs (eux-mêmes) -> souci de réflexivité - JCQ
- Recours au matériel numérique ; coût/bénéfice ; dépendance au contexte indiv./social - JCQ
- Individualisation/personnalisation, en formatif vs. sommatif ; enjeux RGPD/flicage pour étudiants - JCQ
- Ressources vidéos/BD/Illustrations ; formats cours et manque de ressources univ. (e.g., statistiques, reviex SFdS) - JCQ
- Usage de l'IA pour l'auto-évaluation (par HCERES, quoique) ; esprit critique ; affinage de connaissances/compétences - JCQ

---
# EN

- Hybrider un cours (en langues) - EN
- Clés pour monter une télécollaboration à l'international (travail des étudiants avec des pairs à l'étranger) et pour accompagner les étudiants -> étapes de la recherche de partenaires à la sélection des outils, formulation des consignes, quel type d'évaluation préparer, etc. - EN
- Facteurs pour l'engagement des étudiants dans un travail en groupe en ligne -> repérage de facteurs apparaissant de manière récurrente dans la littérature, formation des enseignants à ce sujet ? - EN
- IA générative : pratiques pédagogiques qui en tiennent compte "intelligemment" (en langues) -> repérage et partage de pratiques, développement de stratégies réfléchies du recours à l'IA auprès des étudiants (avec TS) - EN

---
# SC

- Effets des feedbacks (tests/quiz) pendant l'apprentissage (motivation + métacognition) - SC
- "Quatre piliers de l'apprentissage" (S. Dehaene) comme porte d'entrée aux processus d'apprentissage -> pour les étudiant·es pour donner des pistes de stratégies d'apprentissage/révisions - SC
- Stratégies et qualité de la prise de notes des étudiants (papier/ordinateur) - SC
- Principes de conception de documents pédagogiques multimédia (dont vidéos) - SC
- Apport de l'IA (adaptative, pas que générative) en éducation : possibilités et acceptabilité (leviers) - SC 

---
# AS
<!-- _class: t-60 -->

- Focus étudiant. Améliorer/soutenir le sentiment d'appartenance à l'université (en lien avec la santé mentale et l'engagement) - AS
- Focus étudiant et enseignant. Accompagner la classe “inversée” dans les filières en tension avec gros effectifs, sachant que peu d'enthousiasme en ce sens -> comment réussir cette pratique lorsque marginalement mise en place [pas d'expertise] - AS
- Focus étudiant : Accompagner la clairvoyance des étudiants sur les stratégies, méthodes optimales pour réussir à l'université -> peut-être déjà en lien avec le volet “méthodes de travail à l'entrée à l'université [pas d'expertise] - AS
- Focus enseignant : Accompagner les EC vers une réévaluation de la situation face à la surcharge permanente, tout changement peut être vécu comme une contrainte associée à des coûts -> passer à l'envie de changer de petites choses parce que c'est fun et parce qu'on le veut nous (pas pour la politique) [pas d'expertise] -> déjà fait/proposé par DAPI mais pas forcément d'éléments de littérature - AS
- Focus étudiant et enseignant : Accompagner l'utilisation de l'IA générative pour l'apprentissage / Faciliter l'apprentissage pour en faire le meilleur [pas d'expertise mais une réelle préoccupation] -> peut-être déjà en cours volet “méthodes de travail à l'université - AS
  
---
# CdH
- Travaux pratiques en sciences (organisation, coût, apprentissages) {expertise +++ ; intérêt +++} - CH
- Evaluation par grilles critériées (GC) {expertise +++ ; intérêt +}. Lien avec les compétences ? {expertise -} - CH
- Plates-formes péda numériques. Lesquelles ? Pour quelle utilisation ? Publication AMUE “Les LMS dans le supérieur“ {expertise ++ ; intérêt ++} - CH
- Evaluation des productions étudiantes par l'IA {expertise - ; intérêt +++} - CH
- Production de textes par les étudiants aidés par LLM. Projet ANR déposé -> A venir {Expertise - ; intérêt +} - CH

---
# DV

- Salles de classe ambiantes - DV
- Computer-supported collaborative learning - DV
- Quelles connaissances fondamentales enseigner en informatique ? - DV

---
# ML

- Evaluation (notamment le travail collaboratif, en lien avec le développement de l'IA, SAé pour les IUTs, ...) - ML
- Espaces pédagogiques (physiques, virtuels) et pédagogies hybrides - ML
- Pédagogie et TEDS - ML

---
# VZ
- utilisation du jeu / gamification  - VZ
- enseignement hybride / bi-modalité / etc. - VZ
- méthodologie du mémoire avec ou sans IA / plagiat / etc.  - VZ
- travail en équipe / projet - VZ
- l’informatique pour les non informaticiens - VZ

---
# :two: Catégories classées (de la liste précédente)
Voici un classement, dans un ordre non significatif, des propositions ci-dessus.

---
## 1. Vivre à l'université (enseignants/étudiants)
- Focus étudiant. Améliorer/soutenir le sentiment d'appartenance à l'université (en lien avec la santé mentale et l'engagement)
- Focus enseignant : Accompagner les EC vers une réévaluation de la situation face à la surcharge permanente, tout changement peut être vécu comme une contrainte associée à des coûts -> passer à l'envie de changer de petites choses parce que c'est fun et parce qu'on le veut nous (pas pour la politique) [pas d'expertise] -> déjà fait/proposé par DAPI mais pas forcément d'éléments de littérature - AS

---
## 2. Méthodes de travail universitaire
- "Quatre piliers de l'apprentissage" (S. Dehaene) comme porte d'entrée aux processus d'apprentissage -> pour les étudiant·es pour donner des pistes de stratégies d'apprentissage/révisions - SC
- Stratégies et qualité de la prise de notes des étudiants (papier/ordinateur) - SC
- Focus étudiant : Accompagner la clairvoyance des étudiants sur les stratégies, méthodes optimales pour réussir à l'université -> peut-être déjà en lien avec le volet “méthodes de travail à l'entrée à l'université [pas d'expertise] - AS
- Facteurs pour l'engagement des étudiants dans un travail en groupe en ligne -> repérage de facteurs apparaissant de manière récurrente dans la littérature, formation des enseignants à ce sujet ? - EN

---
## 3. Design pédagogique
- Focus étudiant et enseignant. Accompagner la classe “inversée” dans les filières en tension avec gros effectifs, sachant que peu d'enthousiasme en ce sens -> comment réussir cette pratique lorsque marginalement mise en place [pas d'expertise] - AS
- Principes de conception de documents pédagogiques multimédia (dont vidéos) - SC
- Hybrider un cours (en langues) - EN
- enseignement hybride / bi-modalité / etc. - VZ
- Clés pour monter une télécollaboration à l'international (travail des étudiants avec des pairs à l'étranger) et pour accompagner les étudiants -> étapes de la recherche de partenaires à la sélection des outils, formulation des consignes, quel type d'évaluation préparer, etc. - EN

---
## 4. Curriculum
- Quelles connaissances fondamentales enseigner en informatique ? - DV
- l’informatique pour les non informaticiens - VZ
- Pédagogie et TEDS - ML

---
## 5. Méthodes pédagogiques

<!-- _class: t-70 -->
- Travaux pratiques en sciences (organisation, coût, apprentissages) {expertise +++ ; intérêt +++} - CH
- Evaluation par grilles critériées (GC) {expertise +++ ; intérêt +}. Lien avec les compétences ? {expertise -} - CH
- Effets des feedbacks (tests/quiz) pendant l'apprentissage (motivation + métacognition) - SC {Plates-formes}
- Individualisation/personnalisation, en formatif vs. sommatif ; enjeux RGPD/flicage pour étudiants - JCQ
- Evaluation (notamment le travail collaboratif, en lien avec le développement de l'IA, SAé pour les IUTs, ...) - ML
- utilisation du jeu / gamification  - VZ
- travail en équipe / projet - VZ

---
## 6. Plates-formes et ressources pédagogiques
- Plates-formes péda numériques. Lesquelles ? Pour quelle utilisation ? Publication AMUE “Les LMS dans le supérieur“ {expertise ++ ; intérêt ++} - CH
- Interactivité avec ressources/supports ; Bases d'outils/matériels ; Tutoriels vidéos/textes/interactifs (eux-mêmes) -> souci de réflexivité - JCQ
- Recours au matériel numérique ; coût/bénéfice ; dépendance au contexte indiv./social - JCQ
- Ressources vidéos/BD/Illustrations ; formats cours et manque de ressources univ. (e.g., statistiques, review SFdS) - JCQ
- Salles de classe ambiantes - DV
- Espaces pédagogiques (physiques, virtuels) et pédagogies hybrides - ML

---
## 7. IA (principalement générative) et LLM

<!-- _class: t-60 -->

- Evaluation des productions étudiantes par l'IA {expertise - ; intérêt +++} - CH
- Production de textes par les étudiants aidés par LLM. Projet ANR déposé -> A venir {Expertise - ; intérêt +} - CH
- méthodologie du mémoire avec ou sans IA / plagiat / etc.  - VZ
- Focus étudiant et enseignant : Accompagner l'utilisation de l'IA générative pour l'apprentissage / Faciliter l'apprentissage pour en faire le meilleur [pas d'expertise mais une réelle préoccupation] -> peut-être déjà en cours volet “méthodes de travail à l'université - AS
- Usage de l'IA pour l'auto-évaluation (par HCERES, quoique) ; esprit critique ; affinage de connaissances/compétences - JCQ
- IA générative : pratiques pédagogiques qui en tiennent compte "intelligemment" (en langues) -> repérage et partage de pratiques, développement de stratégies réfléchies du recours à l'IA auprès des étudiants (avec Thierry Soubrié) - EN
- Apport de l'IA (adaptative, pas que générative) en éducation : possibilités et acceptabilité (leviers) - SC 
- Computer-supported collaborative learning - DV

---
# :three: Propositions qui sont ressorties 

Après discussion et tri des propositions ci-dessus, voici une liste des propositions pouvant donner lieu, après validation des membres, à la production d'un chapitre. Ici aussi, l'ordre est aléatoire.

---

- Méthodes de travail universitaire et entraînement de la clairvoyance des étudiant·es (JCQ & SC).
- Utilisation de l'IA générative par les étudiant·es pour soutenir leur apprentissage (EN & TS, LIDILEM) -> EFELIA ?
- Les Travaux pratiques en sciences (informatisés ou non) (CdH).
- Le sentiment d'appartenance à l'université des étudiants et son effet sur leur engagement (AS & JB).
- Scholarship of Teaching and Learning dans les services de pédagogie universitaire (PhD & IA).
- Quelles connaissances fondamentales enseigner en informatique ? (DV et XXX)

---
# :four: Suite du travail

1. Amender et valider les thèmes des chapitres de la diapo précédente
2. Commencer la rédaction des chapitres (un exemple sera donné assez vite)
3. Échéancier :
   - **Avril 2025** : Livraison des premiers jets des chapitres pour relectures croisées
   - **Mai 2025** : Organiser une réunion intermédiaire (CS complet ou par thèmes) pour faire un point sur les documents et sur le règlement intérieur du CS
   - **Juin-juillet 2025** : Publication finale